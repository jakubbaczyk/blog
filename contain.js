const e_contain = React.createElement;

class Contain extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        var publication = "";
        if(this.props.containid == "1"){
            var content = "Tutaj mamy pewną treść posta nr jeden.";
        }
        else if(this.props.containid == "2"){
            var content = "W tym miejscu znajduje się prosty tekst wpisu drugiego.";
        }
        else if(this.props.containid == "3"){
            var content = "Kolejny wpis jest juz trzecim wpisem.";
        }
        else if(this.props.containid == "4"){
            var content = "Najnowszy wpis, czyli wpis numer cztery.";
        }
        else if(this.props.containid == "5"){
            var content = "Jeszcze jeden wpis nr 5 tym razem.";
        }
        else {
            var content = "Brak podanego wpisu";
        }
        return (
            React.createElement("p", { className: "contain"} ,content)
        );
    }
}

document.querySelectorAll('.contain')
  .forEach(domContainer => {
    const containid = parseInt(domContainer.dataset.containid, 10);
    ReactDOM.render(
        e_contain(Contain, { containid: containid }),
        domContainer
    );
});