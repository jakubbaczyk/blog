const e_footer = React.createElement;

class Footer extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            React.createElement("div", { width: "100%" },
                React.createElement("p", { className: "footer"} , "Stopka strony blogowej."),
                React.createElement("p", { className: "footer"} , "Strona została stworzona przez Jakuba.")
            )
        );
    }
}

document.querySelectorAll('#footer')
  .forEach(domContainer => {
    ReactDOM.render(
        e_footer(Footer, { }),
        domContainer
    );
});